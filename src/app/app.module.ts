import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WebComponentsExampleComponent } from './web-components-example/web-components-example.component';
import { AngularExampleComponent } from './angular-example/angular-example.component';
import { MaterialLiteExampleComponent } from './material-lite-example/material-lite-example.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatFormFieldModule, MatInputModule, MatButtonModule, MatSelectModule, MatRadioModule, MatCardModule} from "@angular/material";
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    WebComponentsExampleComponent,
    AngularExampleComponent,
    MaterialLiteExampleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,

    // angular example,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
