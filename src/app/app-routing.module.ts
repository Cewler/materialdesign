import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AngularExampleComponent} from "./angular-example/angular-example.component";
import {MaterialLiteExampleComponent} from "./material-lite-example/material-lite-example.component";
import {WebComponentsExampleComponent} from "./web-components-example/web-components-example.component";

const routes: Routes = [
  {
    path: 'angular-example',
    component: AngularExampleComponent,
  },
  {
    path: 'material-lite-example',
    component: MaterialLiteExampleComponent,
  },
  {
    path: 'web-components-example',
    component: WebComponentsExampleComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
