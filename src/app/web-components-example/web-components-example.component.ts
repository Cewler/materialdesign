import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-web-components-example',
  templateUrl: './web-components-example.component.html',
  styleUrls: ['./web-components-example.component.css']
})
export class WebComponentsExampleComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
